<?php

namespace App\Exceptions;

use App\Traits\ApiResponse;
use Exception;
use Illuminate\Support\Facades\Log;

class BaseException extends Exception
{
    use ApiResponse;

    protected $errors = [];
    protected $message = '';
    protected $code;

    public function __construct($code, $message = 'Error', $errors = [])
    {
        $this->code = $code;
        $this->message = $message;
        $this->errors = $errors;

        parent::__construct($message, $code);
    }

    public function render($request)
    {
        return $this->errorResponse($this->code, $this->message, $this->errors);
    }

    public function report()
    {
        Log::emergency($this->message);
    }
}
