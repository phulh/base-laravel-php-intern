<?php

namespace App\Services;

use Illuminate\Support\Facades\Request;

class BaseService
{
    public function __construct()
    {
        //code
    }

    public function all(Request $request)
    {
        //code
    }

    public function getOne($id)
    {
        //code
    }

    public function create(Request $request)
    {
        //code
    }

    public function update($id, Request $request)
    {
        //code
    }

    public function delete($id)
    {
        //code
    }
}
