<?php

namespace App\Traits;

trait ApiResponse
{
    /**
     * @param array $data
     * @param int $statusCode
     * @param string $message
     *
     * @return string
     */
    public function successResponse($data = [], $statusCode, $message = null)
    {
        $response = [
            'success' => true,
            'status_code' => $statusCode,
            'data' => $data,
        ];

        return response()->json($response);
    }

    /**
     * @param int $statusCode
     * @param string $message
     * @param array $errors
     *
     * @return string
     */
    public function errorResponse($statusCode, $message = null, $errors = [])
    {
        $response = [
            'success' => false,
            'status_code' => $statusCode,
            'errors' => ['message' => $message]
        ];

        return response()->json($response);
    }

}
