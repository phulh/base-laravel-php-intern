<?php

// Demo code
if (!function_exists('getRandomNumber')) {
     function getRandomNumber($length) {
         $result = '';
         for($i = 0; $i < $length; $i++) {
             $result .= mt_rand(0, 9);
         }
         return intval($result);
     }
}

// Demo code
if (!function_exists('formatPhoneNumber')) {
    function formatPhoneNumber($phoneCode, $phoneNumber)
    {
        return ltrim($phoneCode, '+') . ltrim($phoneNumber, '0');
    }
}
